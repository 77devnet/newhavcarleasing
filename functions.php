<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
/*if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}*/
function set_request($name, $value)
{
	$_POST[$name] = $value;
	$_GET[$name] = $value;
	$_REQUEST[$name] = $value;
}

function set_page()
{
	$subject = $_SERVER['REQUEST_URI'];
	$pat = "/page\/(\d+)/";
	preg_match($pat, $subject, $matches);
	if (0 < count($matches[1]))
	{
		$page = intval($matches[1]);
		set_request('page', $page);
		set_request('paged', $page);
	}
}

function wcustom_rewrite_basic() {

	$subject = get_pagenum_link();

	$arUrl = explode('/', $subject);

	if (!in_array('car-lease-deals', $arUrl)) return false;

	global $Listing;
	$filterable_categories = $Listing->get_filterable_listing_categories();

	$posts_from_slug = false;

	foreach ($arUrl as $key => $elementUrl)
	{
		$elementUrl = htmlspecialchars($elementUrl);
		if (4 > $key || !$elementUrl) continue;
		if ($key == 4)
		{
			$args = array(
				'name' => $elementUrl,
				'post_type' => 'page'
			);
			$posts_from_slug = get_posts( $args );
			if ($posts_from_slug)
			{
				$post_id = $posts_from_slug[0]->ID;
			}
			foreach ($filterable_categories['make']['terms'] as $make_key => $make_term)
			{
				if (strpos($elementUrl, $make_key) !== false)
				{
					set_request('make', $make_key); continue;
				}
			}

			foreach ($filterable_categories['vehicle-type']['terms'] as $vt_key => $vt_term)
			{
				if (strpos($elementUrl, $vt_key) !== false)
				{
					set_request('vehicle-type', $vt_key); continue;
				}
			}
		}
		if (array_key_exists($elementUrl, $filterable_categories['year']['terms']))
		{
			set_request('yr', $elementUrl); continue;
		}
		if (array_key_exists($elementUrl, $filterable_categories['vehicle-type']['terms']))
		{
			set_request('vehicle-type', $elementUrl); continue;
		}
		if (array_key_exists($elementUrl, $filterable_categories['model']['terms']))
		{
			set_request('model', $elementUrl); continue;
		}
	}

	$post_id = ($post_id) ? $post_id : 179;

	if ($posts_from_slug)
		add_rewrite_rule('car-lease-deals/([^/]+)/([^/]+)?', 'index.php?page_id='.$post_id, 'top');
	else
		add_rewrite_rule('car-lease-deals/([^/]+)/?', 'index.php?page_id='.$post_id, 'top');

	flush_rewrite_rules();
}
add_action('init', 'wcustom_rewrite_basic', 999);
add_action('init', 'set_page', 998);

if ( ! function_exists( 'twentysixteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own twentysixteen_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'twentysixteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentysixteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'twentysixteen' ),
		'social'  => __( 'Social Links Menu', 'twentysixteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'twentysixteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'copyright', 'twentysixteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 1', 'twentysixteen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 2', 'twentysixteen' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own twentysixteen_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentysixteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20160412' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20160412' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20160412' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array( 'twentysixteen-style' ), '20160412' );
	//wp_enqueue_style( 'editor-style', get_template_directory_uri() . '/css/editor-style.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'flaticon', get_template_directory_uri() . '/css/flaticon.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'hover', get_template_directory_uri() . '/css/hover.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'jquery.fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'owl', get_template_directory_uri() . '/css/owl.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', array( 'twentysixteen-style' ), '20160412' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array( 'twentysixteen-style' ), '20160412' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160412', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160412' );
	}
	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'socials', get_template_directory_uri() . '/js/social-likes.min.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'navgoco', get_template_directory_uri() . '/js/navgoco.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'googlemaps', get_template_directory_uri() . '/js/googlemaps.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'jquery.countdown', get_template_directory_uri() . '/js/jquery.countdown.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'jquery.easing.min', get_template_directory_uri() . '/js/jquery.easing.min.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'jquery.fancybox.pack', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'jquery.mixitup.min', get_template_directory_uri() . '/js/jquery.mixitup.min.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'owl.carousel.min', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'respond', get_template_directory_uri() . '/js/respond.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'validate', get_template_directory_uri() . '/js/validate.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'bxslider', get_template_directory_uri() . '/js/bxslider.js', array( 'jquery' ), '20160412', true );
	wp_enqueue_script( 'revolution.min', get_template_directory_uri() . '/js/revolution.min.js', array( 'jquery' ), '20160412', true );

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160412', true );

	wp_localize_script( 'twentysixteen-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'twentysixteen' ),
		'collapse' => __( 'collapse child menu', 'twentysixteen' ),
	) );
	wp_dequeue_script( 'isotope');
	
	// Filter

	global $lwp_options, $post;

	$array = array(
		'ajaxurl'       => admin_url( 'admin-ajax.php' ),
		'current_url'   => get_permalink( get_queried_object_id() ),
		'permalink_set' => (get_option('permalink_structure') ? 'true' : 'false')
	);

	if(isset($lwp_options['comparison_page']) && !empty($lwp_options['comparison_page'])){
		$array['compare'] = get_permalink( $lwp_options['comparison_page'] );
	}


	$id = get_queried_object_id();
	$object = (is_page($id) ? get_page( $id ) : get_post( $id ));

	if( isset($object->post_content) && has_shortcode( $object->post_content, 'listings' ) ){
		$array['on_listing_page'] = "true";
	}

	if(is_singular('listings')){
		global $slider_thumbnails;

		$array['listing_id'] = $post->ID;
		$array['slider_thumb_width'] = $slider_thumbnails['width'];
	}

	if(is_single() || is_page()){
		$array['post_id'] = $post->ID;
	}

	// recaptcha public key
	if($lwp_options['recaptcha_enabled'] == 1 && isset($lwp_options['recaptcha_public_key'])){
		$array['recaptcha_public'] = $lwp_options['recaptcha_public_key'];
		$array['template_url']     = get_template_directory_uri();
	}

	// vehicle terms
	$array['singular_vehicles'] = (isset($lwp_options['vehicle_singular_form']) && !empty($lwp_options['vehicle_singular_form']) ? $lwp_options['vehicle_singular_form'] : __('Vehicle', 'listings') );
	$array['plural_vehicles']   = (isset($lwp_options['vehicle_plural_form']) && !empty($lwp_options['vehicle_plural_form']) ? $lwp_options['vehicle_plural_form'] : __('Vehicles', 'listings') );
	$array['compare_vehicles']  = __("Compare", "listings");

	$array['currency_symbol']    = (isset($lwp_options['currency_symbol']) && !empty($lwp_options['currency_symbol']) ? $lwp_options['currency_symbol'] : "$");
	$array['currency_separator'] = (isset($lwp_options['currency_separator']) && !empty($lwp_options['currency_separator']) ? $lwp_options['currency_separator'] : ".");

	// SSL
	if(is_ssl()){
		$array['is_ssl'] = is_ssl();
	}

	wp_dequeue_script( 'listing_js' );

	wp_enqueue_script( 'listing_js_mod', get_template_directory_uri() . "/js/listing_mod.js", array(), 1, false);
	wp_localize_script( 'listing_js_mod', 'listing_ajax', $array);


	// Filter End
	
	
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );





/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );


if(!function_exists("vehicle_owl")){
	function vehicle_owl($title = "Recent Vehicles", $description = "Browse through the vast selection of vehicles that have been added to our inventory", $limit = -1, $sort = null, $listings = null, $other_options = array()){
		global $lwp_options, $Listing;

		switch($sort){
			case "newest":
				$args  = array("post_type"      => "listings",
							   "posts_per_page" => $limit,
							   "orderby"        => "date",
							   "order"          => "DESC"
						);
				break;

			case "oldest":
				$args  = array("post_type"      => "listings",
							   "posts_per_page" => $limit,
							   "orderby"        => "date",
							   "order"          => "ASC"
						);
				break;

			case "related":
				//
				$args  = array("post_type"      => "listings",
							   "posts_per_page" => $limit,
							   "order"          => "DESC"
						);
				break;

			default:
				$args = array("post_type"		=> "listings",
							  "posts_per_page" 	=> $limit);
				break;
		}

		$data = array();

		// related
		if($sort == "related" && isset($lwp_options['related_category']) && !empty($lwp_options['related_category'])){
			$data[] = array(
				array(
					"key" 	=> $lwp_options['related_category'],
					"value" => $other_options['related_val'],
				)
			);

			unset($other_options['related_val']);
		}


		if($sort == "related" && isset($other_options['current_id']) && !empty($other_options['current_id'])){
			$args['post__not_in'] = array( $other_options['current_id'] );
		}

		if(empty($lwp_options['inventory_no_sold'])){
			$data[] = array("key"     => "car_sold",
							"value" => "2");
		}

		if(!empty($data)){
			$args['meta_query'] = $data;
		}

		$args['post_status'] = 'publish';

		if(isset($listings) && !empty($listings)){
			$listing_ids = explode(",", $listings);
			$args['post__in'] = $listing_ids;
		}

		$query = new WP_Query( $args );

		ob_start(); ?>

	    <div class="recent-vehicles-wrap">
			<div class="row">
	            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 recent-vehicles padding-left-none xs-padding-bottom-20">
	    			<div class="scroller_title margin-top-none"><?php echo $title; ?></div>
	                <p><?php echo $description; ?></p>

	                <div class="arrow3 clearfix" id="slideControls3"><span class="prev-btn"></span><span class="next-btn"></span></div>
	    		</div>
	   			<div class="col-md-10 col-sm-8 padding-right-none sm-padding-left-none xs-padding-left-none">
	   				<?php
	   				$additional_attr = "";
	   				if(!empty($other_options)){
	   					foreach($other_options as $key => $option){
	   						$additional_attr .= "data-" . $key . "='" . $option . "' ";
	   					}
	   				}

	   				?>
					 <div class="inventory_box car_listings boxed boxed_full">
                    	<div class="gallery-slider column-carousel four-column">
						<?php while ( $query->have_posts() ) : $query->the_post();
						global $Listing_Template; 
						$layout = "boxed_fullwidth";
							echo $Listing_Template->locate_template("inventory_listing", array("id" => get_the_ID()));
	                    endwhile; ?>
                        </div><!--end boxed_full-->
	                </div>
	    		</div>

	            <div class="clear"></div>
			</div>
	    </div>
	<?php

	wp_reset_query();

	return ob_get_clean();

	}
}

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}


// blog post
if(!function_exists("blog_post")){
    function blog_post(){ 
    	global $post, $awp_options;
    	
    	$secondary_title = get_post_meta($post->ID, "secondary_title", true);
    	
    	ob_start(); ?>
    	<div class="blog-content margin-bottom-40<?php echo (is_sticky() ? " sticky_post" : ""); ?>">
    		<div class="blog-title">
    			<h2<?php echo (empty($secondary_title) ? " class='margin-bottom-25'" : ""); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    			<?php echo (!empty($secondary_title) ? "<strong class='margin-top-5 margin-bottom-25'>" . $secondary_title. "</strong>" : ""); ?>
            </div>
            <?php if((isset($awp_options['blog_post_details']) && $awp_options['blog_post_details']) || !isset($awp_options['blog_post_details'])){ ?>
    		<ul class="margin-top-10 margin-bottom-15 blog-content-details">
    			<li class="fa fa-calendar"><a href="#"><?php echo get_the_date(); ?></a></li>
    			<li class="fa fa-folder-open">
    			<?php
                $categories      = get_the_category();
                $categories_list = $tooltip_cats = "";
                $cat_inc         = 0;

                if($categories) {
                    foreach($categories as $category) {
                        if($cat_inc < 4){
                            $categories_list .= "<a href='" . get_category_link($category->term_id ) . "'>" . $category->cat_name . "</a>, ";
                        } else {                                
                            $tooltip_cats .= "<a href='" . get_category_link($category->term_id ) . "'>" . $category->cat_name . "</a><br>";
                        }
                                    
                        $cat_inc++;
                    }
                }

                echo (isset($categories_list) && !empty($categories_list) ? substr($categories_list, 0, -2) : "<span>" . __("Not categorized", "automotive") . "</span>");

                // if more than 5
                if(!empty($tooltip_cats)){
                    echo ", <a class='' data-toggle=\"popover\" data-placement=\"top\" data-content=\"" . $tooltip_cats . "\" data-html=\"true\">" . __("More Categories", "automotive") . "...</a>";
                }
                ?>
    			</li>
    			<li class="fa fa-user"><span class="theme_font"><?php _e("Posted by", "automotive"); ?></span> <?php the_author_posts_link(); ?></li>
    			<li class="fa fa-comments"><?php comments_popup_link( __( 'No comments yet', 'automotive' ), __( '1 Comment', 'automotive' ), __( '% Comments', 'automotive' )); ?></li>
    		</ul>
            <?php } ?>
    		<div class="post-entry clearfix">
                <?php
                // blog thumbnail
                if ( has_post_thumbnail() ) {
                    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
                    echo '<div class="featured_blog_post_image"><a href="' . $large_image_url[0] . '" title="' . the_title_attribute( 'echo=0' ) . '" >';
                    echo get_the_post_thumbnail( $post->ID, 'thumbnail' ); 
                    echo '</a></div>';
                } elseif(get_post_type($post) == "listings" && function_exists("auto_image")){
                    global $lwp_options;

                    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
                    $gallery_images  = get_post_meta( $post->ID, "gallery_images", true );

                    if(!empty($gallery_images) && !empty($gallery_images[0])){
                        echo '<div class="featured_blog_post_image"><a href="' . get_permalink( $post->ID ) . '" title="' . the_title_attribute( 'echo=0' ) . '" >';
                        echo auto_image( $gallery_images[0], 'thumbnail' ); 
                        echo '</a></div>';
                    } elseif(empty($gallery_images) && isset($lwp_options['not_found_image']['url'])) {
                        echo '<div class="featured_blog_post_image"><a href="' . get_permalink( $post->ID ) . '" title="' . the_title_attribute( 'echo=0' ) . '" >';
                        echo wp_get_attachment_image_src( $lwp_options['not_found_image']['id'], 'thumbnail' ); 
                        echo '</a></div>';                        
                    }
                } ?>

    		    <?php //echo get_the_excerpt()
                $visual_composer_used = get_post_meta($post->ID, "_wpb_vc_js_status", true);

		        $stripp       = "<br><p><b><u><i><span><a><img>";
				$excerpt      = get_the_excerpt();

                if($visual_composer_used){
	                if(!empty($excerpt)){
		                $post_content = preg_replace( '/\[[^\]]+\]/', '', $excerpt );
	                } else {
		                $post_content = preg_replace( '/\[[^\]]+\]/', '', $excerpt );
	                }
                    $post_content = substr(strip_tags($post_content, $stripp), 0, 1250) . " " . (strlen(strip_tags($post_content, $stripp)) > 1250 ? "[...]" : "");
                } else {
                    $post_content = $excerpt;
                }

                echo $post_content;
                ?>

                <div class="clearfix"></div>

    			<div class="blog-end margin-top-20">
    				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 read-more"><a href="<?php echo get_permalink($post->ID); ?>"><?php _e("Read More", "automotive"); ?>...</a></div>
    				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right post-tags"><span class="fa fa-tags tags">
    				<?php
                    $posttags = get_the_tags();
                    $tags     = $tooltip_tags = "";
                    $tag_inc  = 0;

                    if ($posttags) {
                        foreach($posttags as $tag) {
                            if($tag_inc < 4){
                                $tags .= "<a href='" . get_tag_link($tag->term_id) . "' title='" . $tag->name . " " . __("Tag", "automotive") . "'>" . $tag->name . "</a>, ";
                            } else {
                                $tooltip_tags .= "<a href='" . get_tag_link($tag->term_id) . "' title='" . $tag->name . " " . __("Tag", "automotive") . "'>" . $tag->name . "</a><br>";
                            }
                            
                            $tag_inc++;
                        }                           
                        echo substr($tags, 0, -2);

                        // if more than 5
                        if(!empty($tooltip_tags)){
                            echo ", <a class='' data-toggle=\"popover\" data-placement=\"top\" data-content=\"" . $tooltip_tags . "\" data-html=\"true\">" . __("More Tags", "automotive") . "</a>";
                        }
                    }
                    ?>
    				</span></div>
    				<div class="clearfix"></div>
    			</div>
    		</div>
    	</div>
        <?php
    	
    	$return = ob_get_contents();
    	ob_end_clean();
    	
    	return $return;
    }
}


if(!function_exists("get_current_id")){
    function get_current_id(){
        if(function_exists("is_shop") && is_shop()){
            return get_option("woocommerce_shop_page_id");
        } else {
            return get_queried_object_id();
        }
    }
}

if(!function_exists("content_classes")){
    function content_classes($sidebar){
        // determine classes
        if($sidebar == "left"){
            $return = array("col-lg-9 col-lg-push-3 col-md-push-3 col-md-9 col-sm-7 col-sm-push-5 col-xs-12 padding-right-none padding-left-15 md-padding-left-15 md-padding-right-none sm-padding-left-15 sm-padding-right-none xs-padding-left-none xs-padding-right-none", "col-lg-3 col-lg-pull-9 col-md-pull-9 col-md-3 col-sm-5 col-sm-pull-7 col-xs-12 padding-left-none padding-right-15 md-padding-left-none md-padding-right-15 sm-padding-right-15 sm-padding-left-none xs-padding-left-none xs-padding-right-none xs-padding-top-20 sidebar_left");
        } else if($sidebar == "right"){
            $return = array("col-lg-9 col-md-9 col-sm-9 col-xs-12 padding-right-none padding-left-15 md-padding-right-15 md-padding-left-none sm-padding-right-15 sm-padding-left-none xs-padding-left-none xs-padding-right-none", "col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-left-none padding-right-15 md-padding-right-none md-padding-left-15 sm-padding-left-15 sm-padding-right-none xs-padding-left-none xs-padding-right-none xs-padding-top-20 sidebar_right");
        } else {
            $return = array("col-lg-12 col-md-12 col-sm-12 col-xs-12");
        }

        // 0 = content class
        // 1 = sidebar class

        return $return;
    }
}


//** For live site **//

// Listings export
// register_activation_hook(__FILE__, 'c3m_my_activation');
// add_action('listing_autoexport', 'listings_export');
// function listings_export() {     
// 	require_once( TEMPLATEPATH . '/autoimport/export.php' );
//     $args=array(
//         'content' => 'listings',
//         'status' => 'publish');
//     ob_start();
//     export_wp($args);
//     $xml = ob_get_clean();
//     file_put_contents('listings_import.xml', $xml, FILE_APPEND);
// }

// // Listings media export
// add_action('media_autoexport', 'listing_media_export');
// function listing_media_export() {     
// 	require_once( TEMPLATEPATH . '/autoimport/export_media.php' );
//     $args=array(
//         'content' => 'attachment',
//         'status' => 'publish');
//     ob_start();
//     export_wp($args);
//     $xml = ob_get_clean();
//     file_put_contents('listings_media_import.xml', $xml);
// }
//** For live site **//


// add_action( 'import_listings_media', 'autoimport_listings_media' );
// function autoimport_listings_media() {
//     // get the file
//     require_once TEMPLATEPATH . '/autoimport/wordpress-importer.php';
//     $nwpimport = new WP_Import;
//     $file_import = ABSPATH . 'listings_media_import.xml';
//     //auto_import( $args );
//     set_time_limit(0);
//     $nwpimport->import( $file_import );
// }
//info@newhavencarleasing.com

add_action( 'import_event','import_logs' );
function import_logs(){
	$filename = 'logdata.txt';
	$file_url = array(
			'http://nyccarlease.77dev.net', 
			'http://westpalmbeachcarleasing.77dev.net', 
			'http://autoleasebrooklyn.77dev.net', 
			'http://autoleaselongisland.77dev.net', 
			'http://autoleasingnj.77dev.net',  
			// 'http://bridgeportcarleasing.77dev.net', 
			// 'http://waterburycarleasing.77dev.net', 
			// 'http://bronxcarlease.77dev.net', 
			// 'http://brooklyncarlease.77dev.net', 
			// 'http://carleasebrooklyn.77dev.net',
			// 'http://carleasegardencity.77dev.net',
			// 'http://greenwichcarleasing.77dev.net', 
			// 'http://cheapcarelasenyc.77dev.net',
			// 'http://carleasingqueens.77dev.net', 
			// 'http://carleasenewjersey.77dev.net', 
			// 'http://njcarleasing.77dev.net', 
			// 'http://fortleecarleasing.77dev.net', 
			// 'http://newcardealsnyc.77dev.net', 
			// 'http://jerseycitycarleasing.77dev.net', 
			// 'http://leasecarny.77dev.net',
			// 'http://carleasingbronx.77dev.net', 
			// 'http://fortlauderdalecarleasing.77dev.net', 
			// 'http://cheapcarleasingnyc.77dev.net',
			// 'http://longislandcarlease.77dev.net', 
			// 'http://leasecarnyc.77dev.net', 
			// 'http://newhavencarleasing.77dev.net',
	    );
	//sleep(500);
	if(file_exists($filename) == true) {
		unlink('logdata.txt');
	}
	foreach ($file_url as $value_url) {
		$filen = $value_url.'/listings_import_log.txt';
		$file_headers = @get_headers($filen);
		//$datamail .= print_r($file_headers, true);
		//var_dump($file_headers);
		if($file_headers === false || $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'HTTP/1.1 500 Internal Server Error') {
			$datamail .= 'Error: File not found in'.$filen.'<br/><br/>';
		}
		else {
			$getcontent = file_get_contents($value_url.'/listings_import_log.txt');
			file_put_contents('logdata.txt', $getcontent .PHP_EOL, FILE_APPEND);
		}
	}

	if(file_exists($filename)==true) {
		$lines = htmlspecialchars(file_get_contents($filename));
		$dataex = explode(PHP_EOL, $lines);
		foreach ($dataex as $value) {
		    if(!empty($value)) {
		    		$whatIWant = explode('|',$value);
		      		if(in_array('totalerror', $whatIWant)) {
				        $totalerror = $whatIWant[1];  
				    }
				    if(in_array('errors', $whatIWant)) {
				        $error_lists = html_entity_decode($whatIWant[3]);
				    }
				    if(in_array('success', $whatIWant)) {
				        $success = $whatIWant[11];
				    }
				    if(in_array('updated', $whatIWant)) {
				        $updated = $whatIWant[5];
				    }
				    if(in_array('total_posts', $whatIWant)) {
				        $total_posts = $whatIWant[9];
				    }
				    if(in_array('added', $whatIWant)) {
				        $added = $whatIWant[7];
				    }
				    $sitename = $whatIWant[12];
				    $siteurl = $whatIWant[13];
				    $countall = (int)$success + (int)$totalerror;
				    $datamail .= 'Import report:<br/>
				    Site name: '.$sitename.'<br/>;
				    Site Url: '.$siteurl.'<br/>;
				    Uploaded total: '.$total_posts.' <br/>
				    Updated: '.$updated.'<br/>
				    Added: '.$added.'<br/>
				    Errors '.$totalerror.'<br/><br/>
				    Following cars displayed errors: '.$error_lists.'<br/><hr><br/><br/>';

		    }
		}

	}
	else {
	    $datamail .= 'Files Not Found<br/>';
	}

	$sitename = strtolower( $_SERVER['SERVER_NAME'] );
	$to = get_option('admin_email');
	$subject = 'Import data for copy eautolease sites';
	$message = $datamail;
	$headers .= "Content-type: text/html; charset=".get_bloginfo('charset')."" . "\r\n";
	$headers .= "From: ".$sitename." <wordpress@".$sitename.">" . "\r\n";
	wp_mail( $to, $subject, $message, $headers );

	//echo $datamail;
}

function logdata($file_url){
	if(file_exists($file_url)==true) {
	    $ok = file_get_contents($file_url);
	    $whatIWant = explode('|',$ok);

	    if(in_array('totalerror', $whatIWant)) {
	        $totalerror = $whatIWant[1];  
	    }
	    if(in_array('errors', $whatIWant)) {
	        $error_lists = $whatIWant[3];
	    }
	    if(in_array('success', $whatIWant)) {
	        $success = $whatIWant[11];
	    }
	    if(in_array('updated', $whatIWant)) {
	        $updated = $whatIWant[5];
	    }
	    if(in_array('total_posts', $whatIWant)) {
	        $total_posts = $whatIWant[9];
	    }
	    if(in_array('added', $whatIWant)) {
	        $added = $whatIWant[7];
	    }
	    $countall = (int)$success + (int)$totalerror;
	    $datamail = 'Import report:<br/>
	    Uploaded total: '.$total_posts.' <br/>
	    Updated: '.$updated.'<br/>
	    Added: '.$added.'<br/>
	    Errors '.$totalerror.'<br/><br/>
	    Following cars displayed errors:<br/>'.$error_lists.'<br/>';

	    echo $datamail;
	}
	else {
	    echo 'Error: File not found';
	}
}


/** For copy custom sites **/
add_action('xml_import', 'listing_xml_import');
function listing_xml_import() {  
  $url = "http://eautolease.com/wp/import_files/listings_import.xml";
  $url2 = "http://eautolease.com/wp/import_files/listings_media_import.xml";
  file_put_contents('listings_import.xml',file_get_contents($url));
  file_put_contents('listings_media_import.xml',file_get_contents($url2));
}

add_action( 'import_listings', 'autoimport_listings' );
function autoimport_listings() {
    // get the file
    require_once TEMPLATEPATH . '/autoimport/autoimporter.php';
    $nwp = new WP_Re_Importer;
    $file = ABSPATH . 'listings_import.xml';
    //auto_import( $args );
    $nwp->import( $file );
}

add_action( 'import_listings_media', 'autoimport_listings_media' );
function autoimport_listings_media() {
    // get the file
	require_once TEMPLATEPATH . '/autoimport/wordpress-importer.php';
	// call the function
	$args = array(
			'file'        => ABSPATH . 'listings_media_import.xml',
			'map_user_id' => 1
	);
	auto_import( $args );
}


add_filter( 'http_request_timeout', 'wp9838c_timeout_extend' );
function wp9838c_timeout_extend( $time )
{
    // Default timeout is 5
    return 20;
}


function getrandomprice($pr){
	$r = mt_rand(1,5);
	$array = array('+','-');
	$k = array_rand($array);
	$v = $array[$k];

	$number = $pr;
	$below = 1;
	$above = 5;
	$random = mt_rand(
	    (integer) $number - $r,
	    (integer) $number + $r
	);

	return $random;
}


function generateallprice() {
global $lwp_options, $Listing;
	$option = $Listing->listing_categories;
	$listing_query = get_posts(array(
      'post_type' => 'listings', 
      'posts_per_page'=> -1,
    ));
	foreach ($listing_query as $listing_value) {
		$post_meta = $Listing->get_listing_meta($listing_value->ID);
		$idl = $listing_value->ID;
		$meta = get_post_meta($listing_value->ID);
		$price = $meta['price'][0];
		$randpr = (string)getrandomprice($price);

		$ll = unserialize(unserialize($meta['listing_options'][0]));
		$orgpr = $ll['price']['original'];
		$custom_tax_inside = $ll['custom_tax_inside'];
		$custom_tax_page = $ll['custom_tax_page'];
		$city_mpg = $ll['city_mpg']['value'];
		$highway_mpg = $ll['highway_mpg']['value'];
		$badge_text = $ll['badge_text'];
		$badge_color = $ll['badge_color'];
		$video = $ll['badge_color'];
		$short_desc = $ll['short_desc'];

		$post_options = array(
            "price" => array(
                "value" => $randpr,
                "original" => $orgpr,
            ),
            "custom_tax_inside" => $custom_tax_inside,
            "custom_tax_page" => $custom_tax_page,
            "city_mpg" => array(
            	"value" => $city_mpg,
            ),
            "highway_mpg" => array(
            	"value" => $highway_mpg,
            ),
            "badge_text" => $badge_text,
            "badge_color" => $badge_color,
            "video" => '',
            "short_desc" => $short_desc,
        );		
		$ss = serialize($post_options);
		if(!empty($price) || $price != 0) {
        	//echo '<table><tr><td>'.$listing_value->post_title.' - '.$price.' - '.$randpr.'</td></tr>';
        	update_post_meta($idl, "listing_options", $ss);
        	update_post_meta($idl, "price", $randpr);
        }
	}
}



function get_ip_address() {
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }

    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip))
                    return $ip;
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];

    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip) {
    if (strtolower($ip) === 'unknown')
        return false;

    // generate ipv4 network address
    $ip = ip2long($ip);

    // if the ip is set and not equivalent to 255.255.255.255
    if ($ip !== false && $ip !== -1) {
        // make sure to get unsigned long representation of ip
        // due to discrepancies between 32 and 64 bit OSes and
        // signed numbers (ints default to signed in PHP)
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) return false;
        if ($ip >= 167772160 && $ip <= 184549375) return false;
        if ($ip >= 2130706432 && $ip <= 2147483647) return false;
        if ($ip >= 2851995648 && $ip <= 2852061183) return false;
        if ($ip >= 2886729728 && $ip <= 2887778303) return false;
        if ($ip >= 3221225984 && $ip <= 3221226239) return false;
        if ($ip >= 3232235520 && $ip <= 3232301055) return false;
        if ($ip >= 4294967040) return false;
    }
    return true;
}

/** For copy custom sites **/

