<?php
/*
Plugin Name: Export all sites eautolease
*/

add_action( 'admin_menu', 'generate_xml_menu' );
function generate_xml_menu() {
	add_submenu_page( 'edit.php?post_type=listings', 'Generate xml', 'Generate xml before export', 'manage_options', 'generate_xml', 'generate_sites_xml');
	register_setting( 'generate_sites_xml_settings', 'generate_sites_xml_settings_options' );
}
function generate_sites_xml() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}	

	function listings_export() {     
		require_once( TEMPLATEPATH . '/autoimport/export.php' );
	    $args=array(
	        'content' => 'listings',
	        'status' => 'publish');
	    ob_start();
	    export_wp($args);
	    $xml = ob_get_clean();
	    file_put_contents(ABSPATH.'/import_files/listings_import.xml', $xml);
	}

	// export media
	function listing_media_export() {     
		require_once( TEMPLATEPATH . '/autoimport/export_media.php' );
	    $args=array(
	        'content' => 'attachment',
	        'status' => 'publish');
	    ob_start();
	    export_wp($args);
	    $xml = ob_get_clean();
	    file_put_contents(ABSPATH.'/import_files/listings_media_import.xml', $xml);
	}
	?>
	<div class="wrap">
	 <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
	<?php 
	 	
		if ( !isset( $_GET['settings-updated'] ) ) {
			echo '<h2>Generate xml files for export</h2>';	
			listing_media_export();
 		}

 		if (isset( $_GET['settings-updated'] ) ) {
 			listings_export();
			// add settings saved message with the class of "updated"
			add_settings_error( 'prerror_messages', 'prerror_message', __( 'Xml generated', 'generate_sites_xml_settings' ), 'updated' );
			// show error/update messages
			settings_errors( 'prerror_messages' );
		}	
	?>
	<form action="options.php" method="post">
		<?php
			// output security fields for the registered setting "generate_sites_xml_settings"
			settings_fields( 'generate_sites_xml_settings' );
			
			// output setting sections and their fields
			// (sections are registered for "generate_sites_xml_settings", each field is registered to a specific section)
			do_settings_sections( 'generate_sites_xml_settings' );
			// output save settings button
			submit_button( 'Generate xml' );
		?>
	</form>
	</div>
	<?php
}


add_action( 'admin_menu', 'import_all_sites_menu' );
function import_all_sites_menu() {
	add_submenu_page( 'edit.php?post_type=listings', 'Export all sites', 'Export all sites', 'manage_options', 'import-all-sites', 'import_sites');
	register_setting( 'import_sites_settings', 'import_sites_settings_options' );
}
function import_sites() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}	
	?>
	<div class="wrap">
	 <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
	<?php 

	function http_get($url)
	{
	    $ch = curl_init();    // initialize curl handle
	    curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
	    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
	    curl_setopt($ch, CURLOPT_TIMEOUT, 3); // times out after 4s
	    $result = curl_exec($ch); // run the whole process
	    curl_close($ch); 
	    return($result);
	} //http_get

	 	$sites = array(
			'http://nyccarlease.77dev.net', 
			'http://westpalmbeachcarleasing.77dev.net', 
			'http://autoleasebrooklyn.77dev.net', 
			'http://autoleaselongisland.77dev.net', 
			'http://autoleasingnj.77dev.net',  
			// 'http://bridgeportcarleasing.77dev.net', 
			// 'http://waterburycarleasing.77dev.net', 
			// 'http://bronxcarlease.77dev.net', 
			// 'http://brooklyncarlease.77dev.net', 
			// 'http://carleasebrooklyn.77dev.net',
			// 'http://carleasegardencity.77dev.net',
			// 'http://greenwichcarleasing.77dev.net', 
			// 'http://cheapcarelasenyc.77dev.net',
			// 'http://carleasingqueens.77dev.net', 
			// 'http://carleasenewjersey.77dev.net', 
			// 'http://njcarleasing.77dev.net', 
			// 'http://fortleecarleasing.77dev.net', 
			// 'http://newcardealsnyc.77dev.net', 
			// 'http://jerseycitycarleasing.77dev.net', 
			// 'http://leasecarny.77dev.net',
			// 'http://carleasingbronx.77dev.net', 
			// 'http://fortlauderdalecarleasing.77dev.net', 
			// 'http://cheapcarleasingnyc.77dev.net',
			// 'http://longislandcarlease.77dev.net', 
			// 'http://leasecarnyc.77dev.net', 
			// 'http://newhavencarleasing.77dev.net',
	    );

		if ( !isset( $_GET['settings-updated'] ) ) {
			echo '<h2>List sites will be imported</h2>';	
	     	foreach ($sites as $before) {
	     		echo $before.'<br>';
	     	}
 		}
 		//wp_schedule_single_event( time() + 10, 'import_event' );
 		//import_logs($sites,'logdata.txt');

	    if (isset( $_GET['settings-updated'] ) ) {
			// add settings saved message with the class of "updated"
			add_settings_error( 'prerror_messages', 'prerror_message', __( 'Settings Saved', 'import_sites_settings' ), 'updated' );
			// show error/update messages
			settings_errors( 'prerror_messages' );
			
			foreach ($sites as $sl) {
			   //$response = wp_remote_get($sl.'/?goimport_listing=Z29pbXBvcnRfbGlzdGluZw==', array( 'timeout' => 120, 'httpversion' => '1.0' ) );
				//http_get($sl.'/?goimport_listing=goimport_listing');
			}

			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   echo "Something went wrong:".$error_message;
			} else {
			   echo "Export in the process the cars will be imported within 15 minutes in all sites";
	     	   wp_schedule_single_event( time() + 10, 'import_event' );
			}
		}	

	?>
	<form action="options.php" method="post">
		<?php
			// output security fields for the registered setting "import_sites_settings"
			settings_fields( 'import_sites_settings' );
			
			// output setting sections and their fields
			// (sections are registered for "import_sites_settings", each field is registered to a specific section)
			do_settings_sections( 'import_sites_settings' );
			// output save settings button
			submit_button( 'Export all sites' );
		?>
	</form>
	</div>
	<?php
}